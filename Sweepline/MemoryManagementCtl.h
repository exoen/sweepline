#pragma once
#include "IMemoryManagementCTL.h"
#include "IModel.h"
#include <unordered_set>
#include <queue>
#include <functional>
#include <memory>

using std::unordered_set;
using std::priority_queue;
using std::unique_ptr;
using std::function;
using std::unordered_multiset;

class MemoryManagementCtl : public IMemoryManagementCTL
{
public:
	explicit MemoryManagementCtl(IModel* model);
	~MemoryManagementCtl();
	void addToRoots(vector<int> roots) override;
	void addToRoots(int root) override;
	const vector<int>& getRoots() const override;
	void addUnprocessed(int nodeId) override;
	void addUnprocessed(vector<int> nodeIds) override;
	bool isUnprocessedEmpty() const override;
	void deleteNonPersistentNodesInLayer() override;
	bool isPersistent(int nodeId) const override;
	int minProcessedPeek() const override;
	int minProcessedPop() override;
	void addPersistent(int nodeId) override;

	bool isInNodes(int nodeId) const override;
	void addPersistent(vector<int>& nodes) override;

	bool isRootsEmpty() const override;
	void clearRoots() override;
	void addToLayer(int nodeId) override;
	size_t getCurrentNodePeak() const override;
	size_t getCurrentPersistentNodes() const override;

private:
	void addToNodesInMemory(int nodeId);
	void addToNodesInMemory(vector<int>& nodes);
	void eraseFromNodesInMemory(int nodeId);
	
	vector<int> roots;
	vector<int> layer;
	unordered_multiset<int> nodes;
	unordered_set<int> persistent;
	unique_ptr<priority_queue<int, vector<int>, function<bool(int, int)>>> unprocessed;
	size_t peakNodesInMemory = 0;
	size_t peakPersistentNodes = 0;
};

