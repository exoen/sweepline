#pragma once
#include "ICTLFormula.h"

class FormulaFromFile : public ICTLFormula
{
public:
	FormulaFromFile();
	~FormulaFromFile();
	bool triggersTrace(string predicate) override;
	bool triggersTrace(vector<string> stronglyConnectedComponentPredicates) override;
	bool readFormula(string fileName);

private:
	string formulaType;
	string formulaPredicate;
};

