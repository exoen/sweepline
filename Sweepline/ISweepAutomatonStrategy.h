#pragma once
#include "IAutomaton.h"
#include "IModel.h"
#include "ISweepPersistenceManagement.h"
#include "ITraceAutomatonStrategy.h"

class ITraceAutomatonStategy;

/**
 * \brief Defines the interface for a sweep strategy that performs parallel compositon of model and automaton and provieds a trace if the property is violated 
 */
class ISweepAutomatonStrategy
{
public:
	virtual ~ISweepAutomatonStrategy(){}

	/**
	 * \brief Performs a paralell compositon and traverses the resulting state space in order to determine whether a property is violated
	 * \param model The model representing the system under test
	 * \param automaton Automaton representing the negation of the desired behavior
	 * \param dataManagement Data management needed by the sweep-line method
	 * \param traceStrategy Strategy defining how a trace should be provied
	 * \return boolean whether the property holds or not
	 */
	virtual bool triggersTrace(IModel* model, IAutomaton* automaton, ISweepPersistenceManagement* dataManagement, ITraceAutomatonStategy* traceStrategy) = 0;
};
