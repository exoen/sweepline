#pragma once
#include <unordered_set>
#include <vector>

using std::unordered_set;
using std::vector;

class ITraceCTLStrategy
{
public:
	virtual ~ITraceCTLStrategy() {}

	virtual void appendToSpanningTree(int stateId, int parentId) = 0;
	virtual void appendToSpanningTree(vector<int> stateIds, int parentId) = 0;
	virtual void reportPathFromInitialState(int destinationState) = 0;
	virtual void reportStronglyConnectedComponent(unordered_set<int> stronglyConnectedComponent) = 0;
	virtual void reportViolationFoundMessage() = 0;
	virtual void reportNonMonotonic() = 0;
};
