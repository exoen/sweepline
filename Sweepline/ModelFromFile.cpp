#include "ModelFromFile.h"
#include <sstream>
#include <fstream>
#include <iostream>

using std::ifstream;
using std::istringstream;
using std::get;
using std::make_pair;

ModelFromFile::ModelFromFile() {}
ModelFromFile::~ModelFromFile() {}

bool ModelFromFile::readModelFromFile(string pathToFile)
{
	ifstream file(pathToFile);
	if (file.fail())
		return false;

	string line;
	auto firstLine = true;
	while (getline(file, line))
	{
		istringstream iss(line);
		int state, progressValue;
		iss >> state >> progressValue;
		if (firstLine)
		{
			initialStates.push_back(state);
			firstLine = false;
		}
		vector<pair<string, int>> successors;
		successors.push_back(make_pair("progressValue", progressValue));

		int numberOfPredicates;
		string predicate;
		vector<string> predicates;
		iss >> numberOfPredicates;
		for (auto i = 0; i < numberOfPredicates; i++)
		{
			iss >> predicate;
			predicates.push_back(predicate);
		}
		statePredicates.insert({ state, predicates });

		string action;
		int id;
		while (iss >> action >> id)
			successors.push_back(make_pair(action, id));

		adjacencyList.insert({ state, successors });
	}
	return true;
}

vector<int> ModelFromFile::getInitialStates() const { return initialStates; }

int ModelFromFile::getProgressValue(const int stateId) const
{
	auto found = adjacencyList.find(stateId);
	return found->second.at(0).second;
}

vector<pair<string, int>> ModelFromFile::getSuccessors(const int stateId) const
{
	auto found = adjacencyList.find(stateId);
	if (found == adjacencyList.end())
		return vector < pair<string, int>>();
	vector<pair<string, int>> successors(found->second.begin() + 1, found->second.end());
	return successors;
}

vector<string> ModelFromFile::getPredicatesForState(const int stateId) const
{
	auto found = statePredicates.find(stateId);
	if (found == statePredicates.end())
		return vector<string>();
	return found->second;
}
