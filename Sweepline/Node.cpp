#include "Node.h"

using std::make_pair;

Node::Node() {}

Node::Node(int modelId, int automatonId) { composition = make_pair(modelId, automatonId); }

Node::~Node() {}

int Node::getModelId() const { return composition.first; }

int Node::getAutomatonId() const { return composition.second; }

bool Node::operator==(const Node& other) const { return this->composition == other.composition; }

bool Node::operator!=(const Node& other) const { return !(*this == other); }

istream& operator>> (istream& stream, Node& node) {
	stream >> node.composition.first >> node.composition.second;
	return stream;
}

istream& operator>>(istream& stream, pair<Node, Node>& node) {
	return 	stream >> node.first >> node.second;
}

ostream& operator<<(ostream& os, const Node& node) {
	return os << node.composition.first << " " << node.composition.second;
}

