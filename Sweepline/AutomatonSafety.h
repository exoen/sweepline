#pragma once
#include "ISweepAutomatonStrategy.h"

class AutomatonSafety :
	public ISweepAutomatonStrategy
{
public:
	AutomatonSafety();
	~AutomatonSafety();

	bool triggersTrace(IModel* model, IAutomaton* automaton, ISweepPersistenceManagement* dataManagement, ITraceAutomatonStategy* traceStrategy) override;
};

