#include "Sweepline.h"
#include "ModelFromFile.h"
#include "AutomatonFromFile.h"
#include "getopt.h"
#include "StandardDataImpl.h"
#include "AutomatonSafety.h"
#include "AutomatonTrace.h"
#include "AutomatonLtl.h"

#include <iostream>
#include "ICTLFormula.h"
#include "FormulaFromFile.h"
#include "MemoryManagementCtl.h"
#include "CtlTrace.h"
#include "SweeplineCTL.h"
#include "SafetyCTL.h"
#include "SccCTL.h"

#include <chrono>

using std::string;
using std::endl;
using std::cout;
using std::make_unique;
using std::move;

void showUsage(char* name)
{
	cout << "Usage: " << name << endl
		<< "Options: " << endl
		<< "-m \t File path to model" << endl << endl

		<< "-s \t Check for safety properties" << endl
		<< "-l \t Check for LTL properties" << endl
		<< "-c \t Check for CTL properties" << endl << endl

		<< "-f \t File path to CTL formula" << endl
		<< "-a \t File path to automaton" << endl << endl

		<< "-h \t Show this help message" << endl
		<< endl;
}

int checkCtlProperty(ISweepCTLStrategy* sweepCtlStrategy, string modelFileLocation, string formulaFileLocation)
{
	auto model(make_unique<ModelFromFile>());
	auto formula(make_unique<FormulaFromFile>());
	auto dataManagement(make_unique<MemoryManagementCtl>(model.get()));
	auto ctlTrace(make_unique<CtlTrace>());

	auto mReadSuccess = model->readModelFromFile(modelFileLocation);
	auto fReadSuccess = formula->readFormula(formulaFileLocation);

	if (!(mReadSuccess && fReadSuccess))
	{
		cout << endl << "Error reading from file" << endl;
		if (!mReadSuccess)
			cout << "Unable to open model at filelocation: " << modelFileLocation << endl;
		if (!fReadSuccess)
			cout << "Unable to open formula at filelocation: " << formulaFileLocation << endl;
		return 1;
	}

	SweeplineCTL sweepline(model.get(), sweepCtlStrategy, formula.get(), dataManagement.get(), ctlTrace.get());
	auto startTime = std::chrono::steady_clock::now();
	sweepline.startSweep();
	auto finishTime = std::chrono::steady_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds> (finishTime - startTime);
	cout << "Time CTL: " << elapsed.count() << " milliseconds" << endl;
	return 0;
}

int checkLtlProperty(ISweepAutomatonStrategy* sweepStrategy, string modelFileLocation, string automataFileLocation)
{
	auto model(make_unique<ModelFromFile>());
	auto automata(make_unique<AutomatonFromFile>());
	auto dataManagement(make_unique<StandardDataImpl>((model.get())));
	auto spanningTreeToDisk(make_unique<AutomatonTrace>());

	auto mReadSuccess = model->readModelFromFile(modelFileLocation);
	auto aReadSuccess = automata->readAutomatonFromFile(automataFileLocation);

	if (!(mReadSuccess && aReadSuccess))
	{
		cout << endl << "Error reading from file" << endl;
		if (!mReadSuccess)
			cout << "Unable to open model at filelocation: " << modelFileLocation << endl;
		if (!aReadSuccess)
			cout << "Unable to open automata at filelocation: " << automataFileLocation << endl;
		return 1;
	}

	Sweepline sweepline(sweepStrategy, spanningTreeToDisk.get(), model.get(), automata.get(), dataManagement.get());
	sweepline.startSweep();
	return 0;
}


int main(int argc, char *argv[])
{
	auto startTime = std::chrono::steady_clock::now();

	unique_ptr<ISweepAutomatonStrategy> sweepStrategy(make_unique<AutomatonSafety>());
	unique_ptr<ISweepCTLStrategy> sweepCtlStrategy(make_unique<SafetyCTL>());

	if (argc < 4)
	{
		showUsage(argv[0]);
		return 1;
	}
	string modelFileLocation;
	string automataFileLocation;
	string formulaFileLocation;
	auto ctlChecking = false;
	int c;
	char arguments[] = "hslm:a:cf:";
	while ((c = getopt(argc, argv, arguments)) != -1)
	{
		switch (c)
		{
		case 'h':
			showUsage(argv[0]);
			return 0;
		case 'a':
			automataFileLocation = optarg;
			break;
		case 'm':
			modelFileLocation = optarg;
			break;
		case 's':
			cout << "Checking for safety properties" << endl;
			break;
		case 'l':
			sweepStrategy = make_unique<AutomatonLtl>();
			cout << "Checking for LTL properties" << endl;
			break;
		case 'c':
			sweepCtlStrategy = make_unique<SccCTL>();
			cout << "Checking for CTL properties" << endl;
			break;
		case 'f':
			formulaFileLocation = optarg;
			ctlChecking = true;
			break;
		default:
			cout << "Unknown option!" << endl;
			showUsage(argv[0]);
			return 1;
		}
	}
	cout << modelFileLocation << " ";
	if (ctlChecking)
	{
		cout << formulaFileLocation << endl << endl;
		checkCtlProperty(sweepCtlStrategy.get(), modelFileLocation, formulaFileLocation);
	}

	else
	{
		cout << automataFileLocation << endl << endl;
		checkLtlProperty(sweepStrategy.get(), modelFileLocation, automataFileLocation);
	}
	auto finishTime = std::chrono::steady_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime);
	cout << "Total time spent: " << elapsed.count() << " milliseconds" << endl;
	cout << endl << "State space exploration is completed" << endl;
}
