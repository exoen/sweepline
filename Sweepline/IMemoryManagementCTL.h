#pragma once
#include <vector>
#include <cstddef> // needed by gcc for size_t

using std::vector;

class IMemoryManagementCTL
{
public:
	virtual ~IMemoryManagementCTL() {}

	virtual void addToRoots(vector<int> roots) = 0;
	virtual void addToRoots(int root) = 0;
	virtual const vector<int>& getRoots() const = 0;
	virtual void addUnprocessed(int nodeId) = 0;
	virtual void addUnprocessed(vector<int> nodeIds) = 0;
	virtual bool isUnprocessedEmpty() const = 0;
	virtual void deleteNonPersistentNodesInLayer() = 0;
	virtual bool isPersistent(int nodeId) const = 0;
	virtual int minProcessedPeek() const = 0;
	virtual int minProcessedPop() = 0;
	virtual void addPersistent(int nodeId) = 0;
	virtual void addPersistent(vector<int>& nodes) = 0;
	virtual bool isInNodes(int nodeId) const = 0;
	virtual bool isRootsEmpty() const = 0;
	virtual void clearRoots() = 0;
	virtual void addToLayer(int nodeId) = 0;
	virtual size_t getCurrentNodePeak() const = 0;
	virtual size_t getCurrentPersistentNodes() const = 0;
};
