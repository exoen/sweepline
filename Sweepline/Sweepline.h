#pragma once
#include "IModel.h"
#include "IAutomaton.h"
#include "ISweepPersistenceManagement.h"
#include "ISweepAutomatonStrategy.h"
#include "ITraceAutomatonStrategy.h"

class Sweepline
{
public:
	explicit Sweepline(ISweepAutomatonStrategy* sweepStategy, ITraceAutomatonStategy* traceStategy, IModel* model, IAutomaton* automaton, ISweepPersistenceManagement* dataManagement);
	~Sweepline();

	void startSweep() const;

private:

	IModel* model;
	IAutomaton* automaton;
	ISweepPersistenceManagement* dataManagement;
	ISweepAutomatonStrategy* sweepStategy;
	ITraceAutomatonStategy* traceStrategy;
	vector<Node> initial;
};


