#include "CtlTrace.h"
#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <string>
#include <sstream>

using std::find_if;
using std::ios_base;
using std::endl;
using std::cout;
using std::ifstream;
using std::istream_iterator;
using std::pair;
using std::ostream_iterator;
using std::insert_iterator;
using std::vector;
using std::stringstream;
using std::ios;
using std::for_each;

CtlTrace::CtlTrace()
{
	remove(spanningTreeFileName.c_str());
	remove(traceFileName.c_str());
}

CtlTrace::~CtlTrace() {}

void CtlTrace::appendToSpanningTree(int StateId, int parent)
{
	spanningTreeFile.open(spanningTreeFileName, ios_base::app);
	spanningTreeFile << StateId << " " << parent << endl;
	spanningTreeFile.close();
}

void CtlTrace::appendToSpanningTree(vector<int> stateIds, int parentId)
{
	for (auto it = stateIds.begin(); it != stateIds.end(); ++it)
		appendToSpanningTree(*it, parentId);
}

void CtlTrace::reportPathFromInitialState(int destinationState)
{
	vector<int> trace;
	auto nextToFind = destinationState;

	ifstream file(spanningTreeFileName);
	string line;

	while (nextToFind != -1)
	{
		while (getline(file, line))
		{
			int curr, pred;
			stringstream stream(line);
			stream >> curr >> pred;
			if (curr == nextToFind)
			{
				nextToFind = pred;
				trace.push_back(curr);
				break;
			}
		}
		file.clear();
		file.seekg(0, ios::beg);
	}

	traceFile.open(traceFileName, ios_base::app);
	traceFile << "Start of path: ";
	copy(trace.rbegin(), trace.rend(), ostream_iterator<int>(traceFile, " -> "));
	traceFile << "end of path" << endl;
	traceFile.close();
}

void CtlTrace::reportViolationFoundMessage()
{
	cout << "Formula triggered trace! Writing trace to disk" << endl;
}

void CtlTrace::reportStronglyConnectedComponent(unordered_set<int> stronglyConnectedComponent)
{
	traceFile.open(traceFileName, ios_base::app);
	traceFile << "Start of strongly connected component: ";
	copy(stronglyConnectedComponent.begin(), stronglyConnectedComponent.end(), ostream_iterator<int>(traceFile, " -> "));
	traceFile << "end of strongly connected component" << endl;
}

void CtlTrace::reportNonMonotonic()
{
	cout << "Non-monotonic progress measure detected. Abort" << endl;
}
