#include "StandardDataImpl.h"

using std::make_unique;
StandardDataImpl::StandardDataImpl(IModel* model)
{
	function<bool(Node&, Node&)> cmp = [model](Node& n1, Node& n2) {return model->getProgressValue(n1.getModelId()) > model->getProgressValue(n2.getModelId()); };
	unprocessed = make_unique<priority_queue<Node, vector<Node>, decltype(cmp)>>(cmp);
}

StandardDataImpl::~StandardDataImpl() {}

void StandardDataImpl::addRoots(Node& root)
{
	roots.push_back(root);
	addToNodesInMemory(root);
}

bool StandardDataImpl::isRootsEmpty() const { return roots.empty(); }

void StandardDataImpl::clearRoots()
{
	for (auto i = roots.begin(); i != roots.end(); ++i)
	{
		nodes.erase(*i);
	}
	roots.clear();
}

void StandardDataImpl::addUnprocessed(Node& node)
{
	unprocessed->push(node);
	addToNodesInMemory(node);
}

bool StandardDataImpl::isUnprocessedEmpty() const { return unprocessed->empty(); }

void StandardDataImpl::deleteNonPersistentNodesInLayer()
{
	vector<Node> toDelete;
	for (auto iter = layer.begin(); iter != layer.end(); ++iter)
	{
		if (persistent.find(*iter) == persistent.end())
			toDelete.push_back(*iter);
	}

	for (auto iter = toDelete.begin(); iter != toDelete.end(); ++iter)
		nodes.erase(*iter);

	for (auto iter = toDelete.begin(); iter != toDelete.end(); ++iter)
	{
		if (mppMap.find(*iter) != mppMap.end())
			mppMap.erase(*iter);
	}

	layer.clear();
}

Node StandardDataImpl::minUnprocessedPop()
{
	auto node = unprocessed->top();
	unprocessed->pop();
	return node;
}

const Node& StandardDataImpl::minProcessedPeek() const { return unprocessed->top(); }

bool StandardDataImpl::isInNodes(const Node& node) const { return nodes.find(node) != nodes.end(); }

void StandardDataImpl::addPersistent(Node& node) {
	persistent.insert(node);
	peakPersistentNodes++;
	addToNodesInMemory(node);
}

void StandardDataImpl::addToNodesInMemory(Node& node)
{
	nodes.insert(node);
	if (nodes.size() > peakNodesInMemory)
		peakNodesInMemory = nodes.size();
}

void StandardDataImpl::addToLayer(Node& node)
{
	layer.push_back(node);
	addToNodesInMemory(node);
}

const vector<Node>& StandardDataImpl::getRoots() const { return roots; }

size_t StandardDataImpl::getCurrentNodePeak() const { return peakNodesInMemory; }

size_t StandardDataImpl::getCurrentPersistentNodes() const { return peakPersistentNodes; }

Node* StandardDataImpl::getNodeFromLayer(const Node& node)
{
	auto found = find_if(layer.begin(), layer.end(), [&node](const Node& n) {return n == node; });
	if (found == layer.end())
		return nullptr;
	return &*found;
}

vector<Node> StandardDataImpl::getNodesInMemory()
{
	vector<Node> nodesInMemory;
	for (auto iter = nodes.begin(); iter != nodes.end(); ++iter)
		nodesInMemory.push_back(*iter);
	return nodesInMemory;
}

bool StandardDataImpl::isPersistentEmpty() const { return persistent.empty(); }

void StandardDataImpl::addToMppCanidates(Node node) { mppCandidates.insert(node); }

void StandardDataImpl::deleteFromMppCanidates(Node& node)
{
	auto it = mppCandidates.find(node);
	if (it != mppCandidates.end())
		mppCandidates.erase(it);
}

vector<Node> StandardDataImpl::getPersistentNodes()
{
	vector<Node> v(persistent.begin(), persistent.end());
	return v;
}

bool StandardDataImpl::isInPersistent(const Node& n) const { return persistent.find(n) != persistent.end(); }

vector<Node> StandardDataImpl::getMppCandidates()
{
	vector<Node> nodesD;
	for (auto iter = mppCandidates.begin(); iter != mppCandidates.end(); ++iter)
		nodesD.push_back(*iter);
	return nodesD;
}

bool StandardDataImpl::isMppCanidatesEmpty() { return mppCandidates.empty(); }

void StandardDataImpl::addBlue(Node& node) { blueNodes.insert(node); }

bool StandardDataImpl::isBlue(Node& node) {
	return blueNodes.find(node) != blueNodes.end();
}

void StandardDataImpl::clearBlueNodes() { blueNodes.clear(); }

void StandardDataImpl::addRed(Node& node) { redNodes.insert(node); }

bool StandardDataImpl::isRed(Node& node) { return redNodes.find(node) != redNodes.end(); }

void StandardDataImpl::clearRedNodes() { redNodes.clear(); }

pair<Node, bool> StandardDataImpl::getMppForNode(Node& node)
{
	auto entry = mppMap.find(node);
	if (entry == mppMap.end())
		return std::make_pair(Node(-1, -1), false);

	return entry->second;
}

void StandardDataImpl::addMppForNode(Node& node, pair<Node, bool> mpp) {
	mppMap[node] = mpp;
}

void  StandardDataImpl::addDoneMppCanidate(Node& node)
{
	doneMppCanidates.push_back(node);
}

vector<Node> StandardDataImpl::getDoneMppCanidates()
{
	return doneMppCanidates;
}

void StandardDataImpl::removeDoneMppCanidate(Node& node)
{
	auto it = find(doneMppCanidates.begin(), doneMppCanidates.end(), node);
	if (it != doneMppCanidates.end())
		doneMppCanidates.erase(it);
}
