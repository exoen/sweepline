#pragma once
#include <vector>
#include <cstddef> // needed by gcc for size_t
#include "Node.h"

using std::vector;
using std::pair;

class ISweepPersistenceManagement
{
public:
	virtual ~ISweepPersistenceManagement() {}

	virtual void addRoots(Node& root) = 0;
	virtual bool isRootsEmpty() const = 0;
	virtual const vector<Node>& getRoots() const = 0;
	virtual void clearRoots() = 0;
	virtual void addUnprocessed(Node& node) = 0;
	virtual bool isUnprocessedEmpty() const = 0;
	virtual void deleteNonPersistentNodesInLayer() = 0;
	virtual bool isInPersistent(const Node& n) const = 0;
	virtual Node minUnprocessedPop() = 0;
	virtual const Node& minProcessedPeek() const = 0;
	virtual void addPersistent(Node& node) = 0;
	virtual vector<Node> getPersistentNodes() = 0;
	virtual bool isPersistentEmpty() const = 0;
	virtual bool isInNodes(const Node& node) const = 0;
	virtual vector<Node> getNodesInMemory() = 0;
	virtual void addToLayer(Node& node) = 0;
	virtual void addToMppCanidates(Node node) = 0;
	virtual void deleteFromMppCanidates(Node& node) = 0;
	virtual vector<Node> getMppCandidates() = 0;
	virtual void addDoneMppCanidate(Node& node) = 0;
	virtual vector<Node> getDoneMppCanidates() = 0;
	virtual void removeDoneMppCanidate(Node& node) = 0;
	virtual bool isMppCanidatesEmpty() = 0;
	virtual void addMppForNode(Node& node, pair<Node, bool> mpp) = 0;
	virtual pair<Node, bool> getMppForNode(Node& node) = 0;
	virtual Node* getNodeFromLayer(const Node&  node) = 0;
	virtual size_t getCurrentNodePeak() const = 0;
	virtual size_t getCurrentPersistentNodes() const = 0;
	virtual void addBlue(Node& node) = 0;
	virtual bool isBlue(Node& node) = 0;
	virtual void clearBlueNodes() = 0;
	virtual void addRed(Node& node) = 0;
	virtual bool isRed(Node& node) = 0;
	virtual void clearRedNodes() = 0;
	virtual void addToNodesInMemory(Node& node) = 0;
};
