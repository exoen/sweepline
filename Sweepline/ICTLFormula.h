#pragma once
#include <string>
#include <vector>

using std::string;
using std::vector;

/**
 * \brief Responsible for determing wheter a property is violated based on a predicate
 */
class ICTLFormula
{
public:
	virtual ~ICTLFormula() {}

	/**
	 * \brief Used for safety properties where it is enough to consider a property in a single state at the time
	 * \param predicate Predicate to be checked
	 * \return Boolen whether the predicate triggers a trace
	 */
	virtual bool triggersTrace(string predicate) = 0;
	/**
	 * \brief Used for checking properties based on terminal strongly connected components
	 * \param stronglyConnectedComponentPredicates List of all predicates in a terminal strongly connected component
	 * \return Boolen wheter the predicates triggers a trace
	 */
	virtual bool triggersTrace(vector<string> stronglyConnectedComponentPredicates) = 0;
};
