#pragma once
#include "IAutomaton.h"
#include <vector>
#include <unordered_map>

using std::pair;
using std::vector;
using std::unordered_map;

class AutomatonFromFile :
	public IAutomaton
{
public:
	AutomatonFromFile();
	~AutomatonFromFile();
	bool isAcceptState(int stateId) override;
	vector<int> getSuccessorStates(int stateId, vector<string> actions) override;
	int getInitialState() override;
	bool readAutomatonFromFile(string pathToFile);

private:
	int initialState = -1;
	unordered_map<int, vector<pair<string, int>>> adjacencyList;
	vector<int>  acceptStates;
};

