#include "AutomatonSafety.h"
#include <tuple>
#include <thread>
#include <iostream>

using std::make_pair;
using std::get;

AutomatonSafety::AutomatonSafety() {}

AutomatonSafety::~AutomatonSafety() {}

bool AutomatonSafety::triggersTrace(IModel* model, IAutomaton* automaton, ISweepPersistenceManagement* dataManagement, ITraceAutomatonStategy* traceStrategy)
{
	auto initStates = model->getInitialStates();
	Node preRoot(-1, -1);
	for (auto root : initStates)
	{
		Node nodePair(root, automaton->getInitialState());
		dataManagement->addRoots(nodePair);
		traceStrategy->appendToSpanningTree(nodePair, preRoot);
	}

	while (!dataManagement->isRootsEmpty())
	{
		for (auto root : dataManagement->getRoots())
			dataManagement->addUnprocessed(root);

		dataManagement->clearRoots();

		auto top = dataManagement->minProcessedPeek();
		auto currentProgressValue = model->getProgressValue(top.getModelId());
		while (!dataManagement->isUnprocessedEmpty())
		{
			auto current = dataManagement->minUnprocessedPop();
			
			if (automaton->isAcceptState(current.getAutomatonId()))
			{
				traceStrategy->reportSafetyTraceTriggered();
				traceStrategy->printPath(current);
				return true;
			}

			if (currentProgressValue != model->getProgressValue(current.getModelId()))
			{
				dataManagement->deleteNonPersistentNodesInLayer();
				currentProgressValue = model->getProgressValue(current.getModelId());
			}
			dataManagement->addToLayer(current);

			auto children = model->getSuccessors(current.getModelId());
			for (auto child = children.begin(); child != children.end(); ++child)
			{
				auto statePredicates = model->getPredicatesForState(get<1>(*child));
				auto automatonSuccessors = automaton->getSuccessorStates(current.getAutomatonId(), statePredicates);
				for (auto automatonSuccessor = automatonSuccessors.begin(); automatonSuccessor != automatonSuccessors.end(); ++automatonSuccessor) {
					Node successor(get<1>(*child), *automatonSuccessor);
					if (!dataManagement->isInNodes(successor))
					{
						traceStrategy->appendToSpanningTree(successor, current);

						if (currentProgressValue > model->getProgressValue(get<1>(*child)))
						{
							dataManagement->addPersistent(successor);
							dataManagement->addRoots(successor);
						}
						else
							dataManagement->addUnprocessed(successor);
					}
				}
			}
		}
	}
	return false;
}
