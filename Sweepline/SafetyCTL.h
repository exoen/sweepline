#pragma once
#include "ISweepCTLStrategy.h"
#include "ITraceCTLStrategy.h"

class SafetyCTL : public ISweepCTLStrategy
{
public:
	SafetyCTL();
	~SafetyCTL();
	bool triggersTrace(IModel* _model, ICTLFormula* _formula, IMemoryManagementCTL* _datamanagement, ITraceCTLStrategy* _trace) override;
	bool checkPredicate(const vector<string>& predicates) const;
	
private:
	IModel* model;
	ICTLFormula* formula;
	IMemoryManagementCTL* datamanagement;
	ITraceCTLStrategy* trace;
};

