#pragma once
#include "IModel.h"
#include "ISweepCTLStrategy.h"

class SweeplineCTL
{
public:
	explicit SweeplineCTL(IModel* model, ISweepCTLStrategy* strategy, ICTLFormula* formula, IMemoryManagementCTL* datamanagement, ITraceCTLStrategy* trace);
	~SweeplineCTL();

	void startSweep() const;

private:
	IModel* model;
	ISweepCTLStrategy* sweepStrategy;
	ICTLFormula* formula;
	IMemoryManagementCTL* datamanagement;
	ITraceCTLStrategy* trace;
};




