#include "AutomatonTrace.h"
#include <algorithm>
#include <ostream>
#include <iterator>
#include <string>
#include <sstream>
#include <iostream>
#include <unordered_set>

using std::ifstream;
using std::ios;
using std::stringstream;
using std::cout;
using std::endl;
using std::ios_base;
using std::ostream_iterator;
using std::unordered_set;

Node emptyNode(-1, -1);

AutomatonTrace::AutomatonTrace()
{
	remove(spanningTreeFilename.c_str());
	remove(traceFilename.c_str());
	remove(mppUpdateFileName.c_str());
}

AutomatonTrace::~AutomatonTrace() {}

void AutomatonTrace::printPath(const Node& state)
{
	vector<int> trace;
	auto nextToFind = state;

	ifstream file(spanningTreeFilename);
	string line;

	while (nextToFind != emptyNode)
	{
		Node curr(-2, -2);
		Node pred(-2, -2);
		while (getline(file, line))
		{
			stringstream stream(line);
			stream >> curr >> pred;
			if (curr == nextToFind)
			{
				nextToFind = pred;
				trace.push_back(curr.getModelId());
				break;
			}
		}
		file.clear();
		file.seekg(0, ios::beg);
	}

	traceFile.open(traceFilename, ios_base::app);
	traceFile << "start of path ";
	copy(trace.rbegin(), trace.rend(), ostream_iterator<int>(traceFile, " -> "));
	traceFile << "end of path" << endl;
	traceFile.close();
}

void AutomatonTrace::appendToSpanningTree(const Node& statepair, const Node& parent)
{
	spanningTreefile.open(spanningTreeFilename, ios_base::app);
	spanningTreefile << statepair << " " << parent << endl;
	spanningTreefile.close();
}

void AutomatonTrace::printMlac(const Node& state)
{
	auto id = state.getModelId();
	stack<int> trace;
	unordered_set<int> traceSet;
	trace.push(id);
	traceSet.insert(id);

	auto nextToFind = state;

	ifstream file(mppUpdateFileName);
	string line;

	Node bottomMostCurr(-1, -1);
	Node bottomMostPred(-1, -1);
	do
	{
		while (getline(file, line))
		{
			Node curr(-1, -1);
			Node pred(-1, -1);
			stringstream stream(line);
			stream >> curr >> pred;

			if (curr == nextToFind)
			{
				bottomMostCurr = curr;
				bottomMostPred = pred;
			}
		}
		nextToFind = bottomMostPred;
		trace.push(bottomMostCurr.getModelId());
		traceSet.insert(bottomMostCurr.getModelId());

		file.clear();
		file.seekg(0, ios::beg);
	} while (traceSet.find(nextToFind.getModelId()) == traceSet.end());
	trace.push(nextToFind.getModelId());

	reportTrace(trace);
}

void AutomatonTrace::reportTrace(stack<int>& trace)
{
	vector<int> cycle;
	auto first = trace.top();
	trace.pop();

	cycle.push_back(first);
	auto current = -1;
	while (current != first)
	{
		current = trace.top();
		trace.pop();
		cycle.push_back(current);
	}

	traceFile.open(traceFilename, ios_base::app);
	traceFile << "start of cycle: ";
	copy(cycle.rbegin(), cycle.rend(), ostream_iterator<int>(traceFile, " -> "));
	traceFile << "end of cycle" << endl;
	traceFile.close();
}

void AutomatonTrace::printCycle(stack<int>& cycle)
{
	traceFile.open(traceFilename, ios_base::app);
	traceFile << "start of cycle: ";
	while (!cycle.empty())
	{
		auto c = cycle.top();
		traceFile << c << " -> ";
		cycle.pop();
	}
	traceFile << "end of cycle" << endl;
	traceFile.close();
}

void AutomatonTrace::appendToMppUpdate(const Node& state, const Node& parent)
{
	mppUpdateFile.open(mppUpdateFileName, ios_base::app);
	mppUpdateFile << state << " " << parent << endl;
	mppUpdateFile.close();
}

void AutomatonTrace::reportSlacTraceTriggered()
{
	cout << "Single layer acceptance cycle trace triggered!" << endl;
}

void AutomatonTrace::reportMlacTraceTriggered()
{
	cout << "Multi layer acceptance cycle trace triggered!" << endl;
}

void AutomatonTrace::reportSafetyTraceTriggered()
{
	cout << "Safety property trace triggered" << endl;
}
