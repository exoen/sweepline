#pragma once
#include "IModel.h"
#include <unordered_map>

using std::unordered_map;


class ModelFromFile : public IModel
{
public:
	ModelFromFile();
	~ModelFromFile();
	bool readModelFromFile(string pathToFile);
	vector<int> getInitialStates() const override;
	int getProgressValue(int const stateId) const override;
	vector<pair<string, int>> getSuccessors(const int stateId) const override;

	vector<string> getPredicatesForState(const int stateId) const override;
private:
	unordered_map<int, vector<pair<string, int>>> adjacencyList;
	unordered_map<int, vector<string>> statePredicates;
	vector<int> initialStates;
};

