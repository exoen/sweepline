#pragma once
#include <vector>
#include <string>

using std::string;
using std::vector;
using std::string;
using std::pair;

/**
 * \brief Model representing the system under test
 */
class IModel
{
public:
	virtual ~IModel() {}

	/**
	 * \brief Returns the set of intial states
	 * \return Set of inital states
	 */
	virtual vector<int> getInitialStates() const = 0;

	/**
	 * \brief Returns the progress value for a state
	 * \param stateId Id of the state
	 * \return The progress value
	 */
	virtual int getProgressValue(const int stateId) const = 0;

	/**
	 * \brief Returns the set successor states of a state
	 * \param stateId The id of the state
	 * \return Set of pairs represetenting an action and corresponding successor state id
	 */
	virtual vector<pair<string, int>> getSuccessors(const int stateId) const = 0;

	
	/**
	 * \brief Returns the predicate for a given state
	 * \param stateId the id of the state
	 * \return predicate on that state
	 */
	virtual vector<string> getPredicatesForState(const int stateId) const = 0;
};

