#pragma once
#include "ITraceCTLStrategy.h"
#include <fstream>
#include <unordered_set>

using std::ofstream;
using std::string;
using std::pair;

namespace std {
	inline istream& operator >> (istream& stream, pair<int, int>& state) {
		stream >> state.first >> state.second;
		return stream;
	}
}

class CtlTrace : public ITraceCTLStrategy
{
public:
	CtlTrace();
	~CtlTrace();
	void appendToSpanningTree(int stateId, int parentId) override;
	void appendToSpanningTree(vector<int> stateIds, int parentId) override;
	void reportPathFromInitialState(int destinationState) override;
	void reportViolationFoundMessage() override;
	void reportStronglyConnectedComponent(unordered_set<int> stronglyConnectedComponent) override;
	void reportNonMonotonic() override;

private:
	ofstream spanningTreeFile;
	ofstream traceFile;
	string spanningTreeFileName = "spanningTree.txt";
	string traceFileName = "trace.txt";
};

