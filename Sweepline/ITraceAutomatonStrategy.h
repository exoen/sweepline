#pragma once
#include "Node.h"
#include <vector>
#include <stack>

using std::vector;
using std::stack;

/**
 * \brief The strategy definition to obtain a trace when a property is violated
 */
class ITraceAutomatonStategy
{
public:
	virtual ~ITraceAutomatonStategy() {}

	/**
	 * \brief Append a state to the spanning tree that shall be used to obtain a trace if a property is violated
	 * \param state Node that is appended
	 * \param parent Parent of the state
	 */
	virtual void appendToSpanningTree(const Node& state, const Node& parent) = 0;
	/**
	 * \brief Prints the trace
	 * \param state The state the trace should lead to
	 */
	virtual void printPath(const Node& state) = 0;
	/**
	 * \brief Print info message that a property has been violated
	 */
	virtual void reportSlacTraceTriggered() = 0;
	virtual void reportMlacTraceTriggered() = 0;
	virtual void reportSafetyTraceTriggered() = 0;
	virtual void printCycle(stack<int>& cycle) = 0;
	virtual void printMlac(const Node& state) = 0;
	virtual void appendToMppUpdate(const Node& state, const Node& parent) = 0;
};
