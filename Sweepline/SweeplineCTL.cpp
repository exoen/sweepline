#include "SweeplineCTL.h"

SweeplineCTL::SweeplineCTL(IModel* model, ISweepCTLStrategy* strategy, ICTLFormula* formula, IMemoryManagementCTL* datamanagement, ITraceCTLStrategy* trace)
{
	this->model = model;
	this->sweepStrategy = strategy;
	this->formula = formula;
	this->datamanagement = datamanagement;
	this->trace = trace;
}

SweeplineCTL::~SweeplineCTL() {}

void SweeplineCTL::startSweep() const
{
	sweepStrategy->triggersTrace(model, formula, datamanagement, trace);
}
