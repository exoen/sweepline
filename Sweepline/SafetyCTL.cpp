#include "SafetyCTL.h"
#include "ITraceCTLStrategy.h"

SafetyCTL::SafetyCTL() : model(nullptr), formula(nullptr), datamanagement(nullptr), trace(nullptr) {}

SafetyCTL::~SafetyCTL() {}

bool SafetyCTL::triggersTrace(IModel* _model, ICTLFormula* _formula, IMemoryManagementCTL*_datamanagement, ITraceCTLStrategy* _trace)
{
	model = _model;
	formula = _formula;
	datamanagement = _datamanagement;
	trace = _trace;

	auto initStates = model->getInitialStates();
	datamanagement->addToRoots(initStates);

	trace->appendToSpanningTree(initStates, -1);

	while (!datamanagement->isRootsEmpty())
	{
		auto roots = datamanagement->getRoots();
		datamanagement->addUnprocessed(roots);
		datamanagement->clearRoots();

		auto top = datamanagement->minProcessedPeek();
		auto LayerProgressValue = model->getProgressValue(top);

		while (!datamanagement->isUnprocessedEmpty())
		{
			auto current = datamanagement->minProcessedPop();
			auto currentProgressValue = model->getProgressValue(current);
			auto predicates = model->getPredicatesForState(current);
			if (checkPredicate(predicates))
			{
				trace->reportViolationFoundMessage();
				trace->reportPathFromInitialState(current);
				return true;
			}
			if (LayerProgressValue != currentProgressValue)
			{
				datamanagement->deleteNonPersistentNodesInLayer();
				LayerProgressValue = currentProgressValue;
			}
			datamanagement->addToLayer(current);

			auto children = model->getSuccessors(current);
			for (auto child = children.begin(); child != children.end(); ++child)
			{
				auto successor = child->second;
				if (!datamanagement->isInNodes(successor))
				{
					trace->appendToSpanningTree(successor, current);

					if (LayerProgressValue > model->getProgressValue(successor))
					{
						datamanagement->addPersistent(successor);
						datamanagement->addToRoots(successor);
					}
					else
						datamanagement->addUnprocessed(successor);
				}
			}
		}
	}
	return false;
}

bool SafetyCTL::checkPredicate(const vector<string>& predicates) const
{
	string pred;
	for (auto const&s : predicates)
		pred += s;
	return formula->triggersTrace(pred);
}
