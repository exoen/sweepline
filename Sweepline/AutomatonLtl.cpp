#include "AutomatonLtl.h"
#include <tuple>
#include <memory>

using std::make_pair;
using std::get;
using std::make_unique;

AutomatonLtl::AutomatonLtl() : model(nullptr), automaton(nullptr), dataManagement(nullptr), traceStrategy(nullptr) {}

AutomatonLtl::~AutomatonLtl() {}

bool AutomatonLtl::triggersTrace(IModel* _model, IAutomaton* _automaton, ISweepPersistenceManagement* _dataManagement, ITraceAutomatonStategy* _traceStrategy)
{
	model = _model;
	automaton = _automaton;
	dataManagement = _dataManagement;
	traceStrategy = _traceStrategy;

	auto initStates = model->getInitialStates();

	for (auto state : initStates)
	{
		Node n(state, automaton->getInitialState());
		initial.push_back(n);
		dataManagement->addRoots(n);
		Node preRoot(-1, -1);
		traceStrategy->appendToSpanningTree(n, preRoot);
	}

	while (!dataManagement->isRootsEmpty())
	{
		for (auto r : dataManagement->getRoots())
			dataManagement->addUnprocessed(r);

		dataManagement->clearRoots();

		if (findSlac())
			return true;
	}
	dataManagement->deleteNonPersistentNodesInLayer();
	return findMlac();
}

bool AutomatonLtl::findSlac() const
{
	auto foundSlac = make_unique<bool>(false);
	while (!dataManagement->isUnprocessedEmpty())
	{
		auto layerProgressValue = model->getProgressValue(dataManagement->minProcessedPeek().getModelId());
		auto currentProgressValue = layerProgressValue;
		while (currentProgressValue == layerProgressValue && !dataManagement->isUnprocessedEmpty())
		{
			currentProgressValue = model->getProgressValue(dataManagement->minProcessedPeek().getModelId());
			if (currentProgressValue == layerProgressValue)
			{
				auto state = dataManagement->minUnprocessedPop();
				dataManagement->addToLayer(state);

				if (dfsBlue(state, foundSlac.get()))
					return true;
			}
		}

		dataManagement->deleteNonPersistentNodesInLayer();
		dataManagement->clearBlueNodes();
		dataManagement->clearRedNodes();
	}
	return false;
}

bool AutomatonLtl::dfsBlue(Node& current, bool* foundSlac) const
{
	if (!dataManagement->isBlue(current) && !*foundSlac)
	{
		dataManagement->addBlue(current);

		auto successors = model->getSuccessors(current.getModelId());
		for (auto child = successors.begin(); child != successors.end(); ++child)
		{
			auto automatonSuccessors = automaton->getSuccessorStates(current.getAutomatonId(), model->getPredicatesForState(get<1>(*child)));
			for (auto automatonSuccessor = automatonSuccessors.begin(); automatonSuccessor != automatonSuccessors.end(); ++automatonSuccessor)
			{
				Node successor(get<1>(*child), *automatonSuccessor);
				if (!dataManagement->isInNodes(successor))
					insert(successor, current);

				if (model->getProgressValue(successor.getModelId()) == model->getProgressValue(current.getModelId()))
					dfsBlue(successor, foundSlac);
			}
		}
		if (automaton->isAcceptState(current.getAutomatonId()) && !*foundSlac)
		{
			auto recursionStack = make_unique<stack<int>>();
			if (dfsRed(current, current, recursionStack.get(), foundSlac))
				return true;
		}
	}
	return false;
}

void AutomatonLtl::insert(Node& current, const Node& parent) const
{
	traceStrategy->appendToSpanningTree(current, parent);
	auto layerProgressValue = model->getProgressValue(parent.getModelId());
	auto currentProgressValue = model->getProgressValue(current.getModelId());

	if (currentProgressValue < layerProgressValue)
	{
		dataManagement->addPersistent(current);
		dataManagement->addRoots(current);
	}
	else if (currentProgressValue > layerProgressValue)
		dataManagement->addUnprocessed(current);
	else
		dataManagement->addToLayer(current);
}

bool AutomatonLtl::dfsRed(Node& current, const Node& seed, stack<int>* recursionStack, bool* foundSlac) const
{

	dataManagement->addRed(current);
	recursionStack->push(current.getModelId());

	auto successors = model->getSuccessors(current.getModelId());
	for (auto child = successors.begin(); child != successors.end(); ++child)
	{
		auto automatonSuccessors = automaton->getSuccessorStates(current.getAutomatonId(), model->getPredicatesForState(get<1>(*child)));
		for (auto automatonSuccessor = automatonSuccessors.begin(); automatonSuccessor != automatonSuccessors.end(); ++automatonSuccessor)
		{
			Node successor(get<1>(*child), *automatonSuccessor);

			if (model->getProgressValue(successor.getModelId()) != model->getProgressValue(current.getModelId()))
				continue;

			if (successor == seed && !*foundSlac)
			{
				recursionStack->push(successor.getModelId());
				traceStrategy->reportSlacTraceTriggered();
				traceStrategy->printPath(successor);
				auto s = *recursionStack;
				traceStrategy->printCycle(s);
				*foundSlac = true;
				return true;
			}

			if (!dataManagement->isRed(successor) && !*foundSlac)
				dfsRed(successor, seed, recursionStack, foundSlac);
		}
	}
	recursionStack->pop();
	return false;
}

bool AutomatonLtl::findMlac() const
{
	auto persistentNodes = dataManagement->getPersistentNodes();
	for (auto persitentNode = persistentNodes.begin(); persitentNode != persistentNodes.end(); ++persitentNode)
		dataManagement->addToMppCanidates(*persitentNode);

	while (!dataManagement->isMppCanidatesEmpty())
	{
		auto mppCanidates = dataManagement->getMppCandidates();
		for (auto mppCand = mppCanidates.begin(); mppCand != mppCanidates.end(); ++mppCand)
			dataManagement->addDoneMppCanidate(*mppCand);

		if (computeMpp(mppCanidates))
			return true;

		auto doneMppCandiated = dataManagement->getDoneMppCanidates();
		for (auto iter = doneMppCandiated.begin(); iter != doneMppCandiated.end(); ++iter)
			dataManagement->deleteFromMppCanidates(*iter);


		mppCanidates = dataManagement->getMppCandidates();
		for (auto iter = mppCanidates.begin(); iter != mppCanidates.end(); ++iter)
		{
			if (!dataManagement->getMppForNode(*iter).second)
				dataManagement->deleteFromMppCanidates(*iter);
		}
	}
	return false;
}

bool AutomatonLtl::computeMpp(vector<Node>& mlacCandidates) const
{
	auto allNodes = dataManagement->getNodesInMemory();
	for (auto node = allNodes.begin(); node != allNodes.end(); ++node)
		dataManagement->addMppForNode(*node, make_pair(Node(-1, -1), false));

	for (auto candidate = mlacCandidates.begin(); candidate != mlacCandidates.end(); ++candidate)
	{
		dataManagement->addMppForNode(*candidate, make_pair(*candidate, false));
		traceStrategy->appendToMppUpdate(*candidate, *candidate);
		dataManagement->addUnprocessed(*candidate);
	}

	while (!dataManagement->isUnprocessedEmpty())
	{
		auto layerProgressValue = model->getProgressValue(dataManagement->minProcessedPeek().getModelId());

		while (isInEqualLayer(layerProgressValue))
		{
			auto current = dataManagement->minUnprocessedPop();
			dataManagement->addToLayer(current);
			auto mpp = dataManagement->getMppForNode(current);

			auto prop = make_pair(mpp.first, mpp.second || automaton->isAcceptState(current.getAutomatonId()));

			if (isShadowedByHigherMppCanidate(current, prop))
				dataManagement->removeDoneMppCanidate(current);
			if (prop.first != Node(-1, -1))
				if (mlacVisit(current, prop))
					return true;
		}

		dataManagement->deleteNonPersistentNodesInLayer();
	}
	return false;
}

bool AutomatonLtl::isShadowedByHigherMppCanidate(Node& current, pair<Node, bool>  prop) const
{
	return dataManagement->isInPersistent(current) && prop.first.getModelId() > current.getModelId();
}

bool AutomatonLtl::isInEqualLayer(int layerProgressValue) const
{
	return !dataManagement->isUnprocessedEmpty() && model->getProgressValue(dataManagement->minProcessedPeek().getModelId()) == layerProgressValue;
}

bool AutomatonLtl::mlacVisit(const Node& current, pair<Node, bool> prop) const
{
	auto successors = model->getSuccessors(current.getModelId());
	for (auto child = successors.begin(); child != successors.end(); ++child)
	{
		auto automatonSuccessors = automaton->getSuccessorStates(current.getAutomatonId(), model->getPredicatesForState(get<1>(*child)));
		for (auto automatonSuccessor = automatonSuccessors.begin(); automatonSuccessor != automatonSuccessors.end(); ++automatonSuccessor)
		{
			Node successor(get<1>(*child), *automatonSuccessor);
			if (prop.first == successor && prop.second)
			{
				traceStrategy->appendToMppUpdate(successor, current);
				traceStrategy->reportMlacTraceTriggered();
				traceStrategy->printPath(prop.first);
				traceStrategy->printMlac(prop.first);
				return true;
			}

			if (!dataManagement->isInNodes(successor))
			{
				mlacInsert(successor, current);
			}
			auto successorMpp = dataManagement->getMppForNode(successor);
			if (leftParamIsHigherMpp(prop, successorMpp))
			{
				dataManagement->addMppForNode(successor, make_pair(prop.first, prop.second));
				dataManagement->addUnprocessed(successor);
				traceStrategy->appendToMppUpdate(successor, current);
			}
		}
	}
	return false;
}

bool AutomatonLtl::leftParamIsHigherMpp(pair<Node, bool> prop, pair<Node, bool> successorMpp)
{
	auto propId = prop.first.getModelId();
	auto succId = successorMpp.first.getModelId();

	if (successorMpp.first == Node(-1, -1))
		return true;
	if (propId > succId)
		return true;
	return propId == succId && prop.second && !successorMpp.second;
}

void AutomatonLtl::mlacInsert(Node& current, const Node& parent) const
{
	traceStrategy->appendToSpanningTree(current, parent);
}