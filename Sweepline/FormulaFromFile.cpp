#include "FormulaFromFile.h"
#include <sstream>
#include <fstream>
#include <algorithm>

using std::ifstream;
using std::stringstream;
using std::all_of;
using std::any_of;

FormulaFromFile::FormulaFromFile() {}

FormulaFromFile::~FormulaFromFile() {}

bool FormulaFromFile::triggersTrace(string predicate)
{
	if (formulaType == "ag")
		return predicate != formulaPredicate;
	if (formulaType == "ef")
		return predicate == formulaPredicate;
	return false;
}

bool FormulaFromFile::triggersTrace(vector<string> scc)
{
	auto pred = formulaPredicate;
	if (formulaType == "agef")
		return any_of(scc.begin(), scc.end(), [pred](string current) {return pred != current; });
	if (formulaType == "efag")
		return all_of(scc.begin(), scc.end(), [pred](string current) {return pred == current; });

	return false;
}

bool FormulaFromFile::readFormula(string fileName)
{
	string type;
	string predicate;
	ifstream file(fileName);

	if (file.fail())
		return false;

	string line;
	getline(file, line);
	stringstream ss(line);
	ss >> type >> predicate;

	for (string::size_type i = 0; i < type.length(); ++i)
		type[i] = static_cast<char>(tolower(type[i]));
	for (string::size_type i = 0; i < predicate.length(); ++i)
		predicate[i] = static_cast<char>(tolower(predicate[i]));

	formulaType = type;
	formulaPredicate = predicate;
	return true;
}
