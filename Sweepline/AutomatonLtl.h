#pragma once
#include "ISweepAutomatonStrategy.h"
#include <stack>

using std::stack;

class AutomatonLtl :
	public ISweepAutomatonStrategy
{
public:
	AutomatonLtl();
	~AutomatonLtl();

	bool triggersTrace(IModel* model, IAutomaton* automaton, ISweepPersistenceManagement* dataManagement, ITraceAutomatonStategy* traceStrategy) override;

private:
	bool findSlac() const;
	void mlacInsert(Node& node, const Node& current) const;	
	bool mlacVisit(const Node& node, pair<Node, bool> prop) const;
	bool computeMpp(vector<Node>& states) const;
	static bool leftParamIsHigherMpp(pair<Node, bool> prop, pair<Node, bool> successorMpp);
	bool findMlac() const;
	bool isShadowedByHigherMppCanidate(Node & current, pair<Node, bool> prop) const;
	bool isInEqualLayer(int currentProgressValue) const;
	void insert(Node& node, const Node& parent) const;
	bool dfsBlue(Node& node, bool* foundSlac) const;
	bool dfsRed(Node& node, const Node& seed, stack<int>* stack, bool* foundSlac) const;

	vector<Node> initial;
	IModel* model;
	IAutomaton* automaton;
	ISweepPersistenceManagement* dataManagement;
	ITraceAutomatonStategy* traceStrategy;
};

