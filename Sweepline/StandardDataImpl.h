#pragma once
#include "ISweepPersistenceManagement.h"
#include "IModel.h"
#include <functional>
#include <vector>
#include <queue>
#include <memory>
#include <unordered_set>
#include <algorithm>
#include <unordered_map>

namespace std {
	template<> struct hash<Node>
	{
		size_t operator()(const Node& node) const {
			return hash<int>{}(node.getModelId());
		}
	};
}

using std::unordered_set;
using std::unordered_map;
using std::unique_ptr;
using std::vector;
using std::function;
using std::pair;
using std::find_if;
using std::priority_queue;
using std::unordered_multiset;

class StandardDataImpl :
	public ISweepPersistenceManagement
{
public:
	explicit StandardDataImpl(IModel* model);
	~StandardDataImpl();

	void addRoots(Node& root) override;
	bool isRootsEmpty() const override;
	const vector<Node>& getRoots() const override;
	void clearRoots() override;
	void addUnprocessed(Node& node) override;
	bool isUnprocessedEmpty() const override;
	void deleteNonPersistentNodesInLayer() override;
	Node minUnprocessedPop() override;
	const Node& minProcessedPeek() const override;
	void addPersistent(Node& node) override;
	bool isInNodes(const Node& node) const override;
	void addToLayer(Node& node) override;
	size_t getCurrentNodePeak() const override;
	size_t getCurrentPersistentNodes() const override;
	Node* getNodeFromLayer(const Node& node) override;

	vector<Node> getNodesInMemory() override;
	bool isPersistentEmpty() const override;

	void addToMppCanidates(Node node) override;

	void deleteFromMppCanidates(Node& node) override;
	vector<Node> getPersistentNodes() override;
	bool isInPersistent(const Node& n) const override;

	vector<Node> getMppCandidates() override;
	bool isMppCanidatesEmpty() override;

	void addBlue(Node& node) override;
	bool isBlue(Node& node) override;
	void clearBlueNodes() override;
	void addRed(Node& node) override;
	bool isRed(Node& node) override;
	void clearRedNodes() override;

	pair<Node, bool> getMppForNode(Node& node) override;
	void addMppForNode(Node& node, pair<Node, bool> mpp) override;
	void addDoneMppCanidate(Node& node) override;
	vector<Node> getDoneMppCanidates() override;
	void removeDoneMppCanidate(Node& node) override;
private:
	void addToNodesInMemory(Node& node);
	vector<Node> roots;
	vector<Node> layer;
	unordered_multiset<Node> nodes;
	unordered_set<Node> persistent;
	unordered_set<Node> mppCandidates;
	unordered_set<Node> blueNodes;
	unordered_set<Node> redNodes;
	unordered_map < Node, pair<Node, bool>> mppMap;
	vector<Node> doneMppCanidates;

	unique_ptr<priority_queue<Node, vector<Node>, function<bool(Node&, Node&)>>> unprocessed;
	size_t peakNodesInMemory = 0;
	size_t peakPersistentNodes = 0;
};
