#pragma once
#include "ISweepCTLStrategy.h"
#include <unordered_map>
#include <unordered_set>
#include <stack>
#include <memory>

using std::stack;
using std::unordered_map;
using std::unordered_set;
using std::unique_ptr;

class SccCTL : public ISweepCTLStrategy
{
public:
	SccCTL();
	~SccCTL();
	void sccDfs(int current, unordered_set<int>& discovered, const unordered_set<int>& component, bool* violatesTerminalness) const;
	bool isSccTerminal(const unordered_set<int>& stronglyConnectedComponent) const;
	bool SCCUtil(int current, unordered_map<int, int> *disc, unordered_map<int, int>* low, unordered_map<int, bool>* stackMember, stack<int> *stack, int currentProgressValue, bool* traceTriggered) const;
	bool triggersTrace(IModel* model, ICTLFormula* formula, IMemoryManagementCTL* datamanagement, ITraceCTLStrategy* trace) override;

private:
	IModel* model;
	ICTLFormula* formula;
	IMemoryManagementCTL* datamanagement;
	ITraceCTLStrategy* trace;
};

