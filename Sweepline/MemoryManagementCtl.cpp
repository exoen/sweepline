#include "MemoryManagementCtl.h"
#include "IModel.h"

using std::make_unique;

MemoryManagementCtl::MemoryManagementCtl(IModel* model)
{
	function<bool(int, int)> cmp = [model](int state1, int state2) {return model->getProgressValue(state1) > model->getProgressValue(state2); };
	unprocessed = make_unique<priority_queue<int, vector<int>, decltype(cmp)>>(cmp);
}


MemoryManagementCtl::~MemoryManagementCtl() {}

void MemoryManagementCtl::addToRoots(vector<int> rootsToAdd) {
	roots.insert(roots.end(), rootsToAdd.begin(), rootsToAdd.end());
	addToNodesInMemory(rootsToAdd);
}

void MemoryManagementCtl::addToRoots(int root)
{
	roots.push_back(root);
	addToNodesInMemory(root);
}

const vector<int>& MemoryManagementCtl::getRoots() const { return roots; }

void MemoryManagementCtl::addUnprocessed(int nodeId)
{
	unprocessed->push(nodeId);
	addToNodesInMemory(nodeId);
}

void MemoryManagementCtl::addUnprocessed(vector<int> nodeIds)
{
	for (auto it = nodeIds.begin(); it != nodeIds.end(); ++it)
		addUnprocessed(*it);
}

bool MemoryManagementCtl::isUnprocessedEmpty() const { return unprocessed->empty(); }

void MemoryManagementCtl::deleteNonPersistentNodesInLayer()
{
	vector<int> toDelete;
	for (auto iter = layer.begin(); iter != layer.end(); ++iter)
	{
		if (persistent.find(*iter) == persistent.end())
			toDelete.push_back(*iter);
	}

	for (auto iter = toDelete.begin(); iter != toDelete.end(); ++iter)
		eraseFromNodesInMemory(*iter);


	layer.clear();
}

bool MemoryManagementCtl::isPersistent(int stateId) const { return persistent.find(stateId) != persistent.end(); }

int MemoryManagementCtl::minProcessedPeek() const { return unprocessed->top(); }

int MemoryManagementCtl::minProcessedPop()
{
	auto node = unprocessed->top();
	unprocessed->pop();
	return node;
}

void MemoryManagementCtl::addPersistent(int stateId)
{
	persistent.insert(stateId);
	peakPersistentNodes++;
	addToNodesInMemory(stateId);
}

void MemoryManagementCtl::addPersistent(vector<int>& nodesToAdd) {
	persistent.insert(nodesToAdd.begin(), nodesToAdd.end());
	addToNodesInMemory(nodesToAdd);
}

void MemoryManagementCtl::addToNodesInMemory(int stateId)
{
	nodes.insert(stateId);
	if (nodes.size() > peakNodesInMemory)
		peakNodesInMemory = nodes.size();
}

void MemoryManagementCtl::addToNodesInMemory(vector<int>& statesToAdd) {
	for (auto i = statesToAdd.begin(); i != statesToAdd.end(); ++i)
		addToNodesInMemory(*i);
}

void MemoryManagementCtl::eraseFromNodesInMemory(int nodeId)
{
	if (isInNodes(nodeId))
		nodes.erase(nodeId);
}

bool MemoryManagementCtl::isRootsEmpty() const { return roots.empty(); }

void MemoryManagementCtl::clearRoots()
{
	for (auto i = roots.begin(); i != roots.end(); ++i)
		eraseFromNodesInMemory(*i);

	roots.clear();
}

void MemoryManagementCtl::addToLayer(int stateId)
{
	layer.push_back(stateId);
	addToNodesInMemory(stateId);
}

size_t MemoryManagementCtl::getCurrentNodePeak() const
{
	return peakNodesInMemory;
}

size_t MemoryManagementCtl::getCurrentPersistentNodes() const
{
	return peakPersistentNodes;
}

bool MemoryManagementCtl::isInNodes(int stateId) const {
	return nodes.find(stateId) != nodes.end();
}




