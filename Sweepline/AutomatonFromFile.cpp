#include "AutomatonFromFile.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <unordered_set>

using std::ifstream;
using std::istringstream;
using std::all_of;

AutomatonFromFile::AutomatonFromFile() {}

AutomatonFromFile::~AutomatonFromFile() {}

int AutomatonFromFile::getInitialState() { return initialState; }

bool AutomatonFromFile::readAutomatonFromFile(string pathToFile)
{
	ifstream file(pathToFile);
	if (file.fail())
		return false;

	string line;
	getline(file, line);
	istringstream is1(line);
	int init;
	is1 >> init;
	initialState = init;

	getline(file, line);
	istringstream is2(line);
	int acceptState;
	while (is2 >> acceptState)
		acceptStates.push_back(acceptState);

	while (getline(file, line))
	{
		int stateId;
		istringstream iss(line);
		pair<string, int> successor;
		vector<pair<string, int>> successors;

		iss >> stateId;
		while (iss >> successor.first >> successor.second)
			successors.push_back(successor);

		adjacencyList.insert({ stateId,successors });
	}
	return true;
}

bool AutomatonFromFile::isAcceptState(int stateId) {
	return find(acceptStates.begin(), acceptStates.end(), stateId) != acceptStates.end();
}

vector<int> AutomatonFromFile::getSuccessorStates(int stateId, vector<string> actions)
{
	std::unordered_set<int> successors;
	string allActions;
	auto state = adjacencyList.find(stateId);

	vector<string> actionsToBeChecked;
	for (auto action = actions.begin(); action != actions.end(); ++action)
	{
		actionsToBeChecked.push_back(*action);
		allActions += *action;
	}
	actionsToBeChecked.push_back(allActions);

	for (auto transition = state->second.begin(); transition != state->second.end(); ++transition)
	{
		for (auto action = actionsToBeChecked.begin(); action != actionsToBeChecked.end(); ++action)
		{
			if (*action == transition->first || transition->first == "true")
				successors.insert(transition->second);
		}
	}
	return vector<int>(successors.begin(), successors.end());
}
