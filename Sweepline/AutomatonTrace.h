#pragma once
#include "ITraceAutomatonStrategy.h"
#include <fstream>
#include <stack>

using std::ofstream;
using std::string;

class AutomatonTrace :
	public ITraceAutomatonStategy
{
public:
	AutomatonTrace();
	~AutomatonTrace();
	void printPath(const Node& state) override;
	void appendToSpanningTree(const Node& state, const Node& parent) override;
	void printCycle(stack<int>& cycle) override;
	void reportTrace(stack<int>& trace);
	void printMlac(const Node& state) override;
	void appendToMppUpdate(const Node& state, const Node& parent) override;
	void reportSlacTraceTriggered() override;
	void reportMlacTraceTriggered() override;
	void reportSafetyTraceTriggered() override;

private:
	ofstream spanningTreefile;
	ofstream traceFile;
	ofstream mppUpdateFile;
	string spanningTreeFilename = "spanningTree.txt";
	string traceFilename = "trace.txt";
	string mppUpdateFileName = "mppUpdate.txt";
};

