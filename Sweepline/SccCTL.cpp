#include "SccCTL.h"
#include <memory>
#include <algorithm>
#include <unordered_set>

using std::unique_ptr;
using std::min;
using std::find;
using std::make_unique;

SccCTL::SccCTL() : model(nullptr), formula(nullptr), datamanagement(nullptr), trace(nullptr) {}

SccCTL::~SccCTL() {}

bool SccCTL::triggersTrace(IModel* mModel, ICTLFormula* mFormula, IMemoryManagementCTL* mDatamanagement, ITraceCTLStrategy* mTrace)
{
	this->model = mModel;
	this->formula = mFormula;
	this->datamanagement = mDatamanagement;
	this->trace = mTrace;

	auto initStates = model->getInitialStates();
	datamanagement->addToRoots(initStates);
	trace->appendToSpanningTree(initStates, -1);

	auto disc = make_unique<unordered_map<int, int>>();
	auto low = make_unique<unordered_map<int, int>>();
	auto stackMember = make_unique<unordered_map<int, bool>>();
	auto recStack = make_unique<stack<int>>();
	auto traceTriggered = make_unique<bool>(false);


	auto roots = datamanagement->getRoots();
	datamanagement->addUnprocessed(roots);
	datamanagement->clearRoots();

	auto top = datamanagement->minProcessedPeek();
	auto currentProgressValue = model->getProgressValue(top);

	while (!datamanagement->isUnprocessedEmpty())
	{
		auto current = datamanagement->minProcessedPop();
		if (currentProgressValue < model->getProgressValue(current))
		{
			datamanagement->deleteNonPersistentNodesInLayer();
			currentProgressValue = model->getProgressValue(current);
			disc->clear();
			low->clear();
			stackMember->clear();
			while (!recStack->empty())
				recStack->pop();
		}
		if (disc->find(current) == disc->end())
			if (SCCUtil(current, disc.get(), low.get(), stackMember.get(), recStack.get(), currentProgressValue, traceTriggered.get()))
				return true;
	}
	return false;
}

bool SccCTL::SCCUtil(int current, unordered_map<int, int> *disc, unordered_map<int, int>* low, unordered_map<int, bool>* stackMember, stack<int>* stack, int currentProgressValue, bool* traceTriggered) const
{
	static auto time = 0;
	time++;
	disc->insert({ current,time });
	low->insert({ current,time });
	stack->push(current);
	stackMember->insert({ current,true });
	datamanagement->addToLayer(current);

	auto children = model->getSuccessors(current);
	for (auto iter = children.begin(); iter != children.end(); ++iter)
	{
		auto child = iter->second;
		if (!datamanagement->isInNodes(child))
		{
			trace->appendToSpanningTree(child, current);
			if (currentProgressValue < model->getProgressValue(child))
			{
				datamanagement->addUnprocessed(child);
				continue;
			}
			if (currentProgressValue > model->getProgressValue(child))
			{
				*traceTriggered = true;
				trace->reportNonMonotonic();
				return false;
			}
			if (!*traceTriggered)
			{
				SCCUtil(child, disc, low, stackMember, stack, currentProgressValue, traceTriggered);
				low->at(current) = min(low->at(current), low->at(child));
			}
		}
		if (stackMember->find(child) != stackMember->end() && stackMember->at(child)) {
			low->at(current) = min(low->at(current), disc->at(child));
		}
	}

	if (!*traceTriggered && low->at(current) == disc->at(current))
	{
		auto sccState = 0;
		unordered_set<int> scc;
		while (stack->top() != current)
		{
			sccState = stack->top();
			scc.insert(sccState);
			stackMember->at(sccState) = false;
			stack->pop();
		}
		sccState = stack->top();
		scc.insert(sccState);
		stackMember->at(sccState) = false;
		stack->pop();
		if (isSccTerminal(scc))
		{
			vector<string> predicatesInScc;
			for (auto state = scc.begin(); state != scc.end(); ++state)
			{
				auto predicates = model->getPredicatesForState(*state);
				for (auto pred = predicates.begin(); pred != predicates.end(); ++pred)
				{
					predicatesInScc.push_back(*pred);
				}
			}
			if (formula->triggersTrace(predicatesInScc))
			{
				*traceTriggered = true;
				trace->reportViolationFoundMessage();
				trace->reportPathFromInitialState(current);
				trace->reportStronglyConnectedComponent(scc);
				return true;
			}
		}
	}
	return false;
}

bool SccCTL::isSccTerminal(const unordered_set<int>& scc) const
{
	unordered_set<int> discovered;
	auto isTerminal = make_unique<bool>(true);
	auto start = *scc.begin();

	sccDfs(start, discovered, scc, isTerminal.get());
	return *isTerminal.get();
}

void SccCTL::sccDfs(int current, unordered_set<int>& discovered, const unordered_set<int>& component, bool* isTerminal) const
{
	discovered.insert(current);
	auto children = model->getSuccessors(current);
	for (auto child = children.begin(); child != children.end(); ++child)
	{
		if (component.find(child->second) == component.end())
			*isTerminal = false;

		if (*isTerminal && discovered.find(child->second) == discovered.end())
			sccDfs(child->second, discovered, component, isTerminal);
	}
}