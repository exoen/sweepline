#include "Sweepline.h"
#include "IModel.h"
#include "ISweepPersistenceManagement.h"
#include "ISweepAutomatonStrategy.h"
#include "ITraceAutomatonStrategy.h"


Sweepline::Sweepline(ISweepAutomatonStrategy* sweepStategy, ITraceAutomatonStategy* traceStategy, IModel* model, IAutomaton* automaton, ISweepPersistenceManagement *dataManagement)
{
	this->automaton = automaton;
	this->model = model;
	this->dataManagement = dataManagement;
	this->sweepStategy = sweepStategy;
	this->traceStrategy = traceStategy;
}

Sweepline::~Sweepline() {}

void Sweepline::startSweep() const
{
	sweepStategy->triggersTrace(model, automaton, dataManagement, traceStrategy);
}