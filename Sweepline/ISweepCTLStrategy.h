#pragma once
#include "IModel.h"
#include "ICTLFormula.h"
#include "IMemoryManagementCTL.h"
#include "ITraceCTLStrategy.h"


/**
 * \brief Defines the interface for a sweep strategy for CTL property checking
 */
class ISweepCTLStrategy
{
public:
	virtual ~ISweepCTLStrategy(){}

	/**
	 * \brief Performs a verification of a model an a CTL formula
	 * \param model The model representing the system under test
	 * \param formula formula to be verified
	 * \param datamanagement Data management needed by the sweep-line method
	 * \param trace Strategy defining how a trace should be provied
	 * \return boolean whether the property holds
	 */
	virtual bool triggersTrace(IModel* model, ICTLFormula* formula, IMemoryManagementCTL* datamanagement, ITraceCTLStrategy* trace) = 0;
};
