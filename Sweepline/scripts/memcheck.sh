#!/bin/bash

now=`date +%d-%m-%Y`
model=$1
automata=$2
verificationType=$3
predFlag=$4
echo "Model: $model, automata: $automata"

make memcheck

cd ../

valgrind --leak-check=yes --leak-check=full --show-leak-kinds=all --log-file=profilingOutput/memcheck$now.txt ./a.out -m $model $predFlag $automata $verificationType 

rm a.out
