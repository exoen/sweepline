#!bin/bash

now=`date +%d-%m-%Y`
tempfile=scripts/cachegrind.txt

model=$1
automata=$2
verificationType=$3
predFlag=$4
echo "Model: $model, automata: $automata"

make cachegrind

cd ../

valgrind --tool=cachegrind --cachegrind-out-file=$tempfile ./a.out -m $model $predFlag $automata $verificationType

rm a.out

cg_annotate $tempfile > profilingOutput/cachegrind$now.txt

cd scripts
