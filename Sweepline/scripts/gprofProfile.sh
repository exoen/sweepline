#!/bin/bash

now=`date +%d-%m-%Y`
model=$1
automata=$2
verificationType=$3
predFlag=$4
echo "Model: $model, automata: $automata"

make gprof

cd ../

./a.out -m $model $predFlag $automata $verificationType > temp.txt

gprof a.out gmon.out > profilingOutput/gprof$now.txt

rm a.out gmon.out temp.txt

cd scripts/
