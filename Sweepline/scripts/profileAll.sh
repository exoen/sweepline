#!/bin/bash

OPTIND=1
model=""
automata=""
verificationType=""
usePred=""

while getopts "a:m:t:p:" opt; do
  case "$opt" in
    a) automata=$OPTARG
    ;;
    m) model=$OPTARG
    ;;
    t)  verificationType=$OPTARG
	;;
	p) usePred=$OPTARG
  esac
done

echo "Automata=$automata, model=$model, verification flag=$verificationType, usePred flag=$usePred"

echo "Gprof: "
sh gprofProfile.sh $model $automata $verificationType -$usePred

echo "Memcheck: "
sh memcheck.sh $model $automata $verificationType -$usePred

echo "Massif: "
sh massif.sh $model $automata $verificationType -$usePred

echo "Callgrind: "
sh callgrind.sh $model $automata $verificationType -$usePred

echo "Cachegrind: "
sh cachegrind.sh $model $automata $verificationType -$usePred
