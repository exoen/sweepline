#!bin/bash

out=callgrind.out
now=`date +%d-%m-%Y`
model=$1
automata=$2
verificationType=$3
predFlag=$4

echo "Model: $model, automata: $automata"

make callgrind

cd ../

valgrind --tool=callgrind --callgrind-out-file=$out ./a.out -m $model $predFlag $automata $verificationType

rm a.out

callgrind_annotate $out > profilingOutput/callgrind$now.txt

rm $out

cd scripts
