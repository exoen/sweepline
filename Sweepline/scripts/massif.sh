now=`date +%d-%m-%Y`
massifTmp=massifOut.txt
model=$1
automata=$2
verificationType=$3
predFlag=$4
echo "Model: $model, automata: $automata"

make massif

cd ../

valgrind --tool=massif --massif-out-file=$massifTmp ./a.out -m $model $predFlag $automata $verificationType

ms_print $massifTmp > profilingOutput/massif$now.txt

rm a.out $massifTmp

cd scripts
