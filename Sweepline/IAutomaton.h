﻿#pragma once
#include <string>
#include <vector>

using std::string;
using std::vector;

/**
 * \brief Representation of the automaton used to express the negated property of interest
 */
class IAutomaton
{
	
public:
	virtual ~IAutomaton() {}

	/**
	 * \brief Determines whether the automaton is in an accept state
	 * \param id The id of the state
	 * \return boolean
	 */
	virtual bool isAcceptState(int id) = 0;

	/**
	 * \brief Returns a set of sucessor states
	 * \param action The action done by the system
	 * \return The id of the successor state accouring to the action perfomed
	 */
	virtual vector<int> getSuccessorStates(int currentState, vector<string> action) = 0;

	/**
	 * \brief Returns Id for the intial state
	 * \return Id for initial state
	 */
	virtual int getInitialState() = 0;
};
