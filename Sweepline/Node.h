#pragma once
#include <utility>
#include <istream>
#include <string>
#include <iostream>

using std::pair;
using std::istream;
using std::ostream;

class Node
{
public:
	Node();
	Node(int modelId, int automatonId);
	~Node();
	int getModelId() const;
	int getAutomatonId() const;
	bool operator==(const Node& other) const;
	bool operator!=(const Node& other) const;
	friend istream& operator>> (istream& stream, Node& node);
	friend istream& operator>> (istream& stream, pair<Node, Node>& node);
	friend ostream& operator<< (ostream& os, const Node& node);

private:
	pair<int, int> composition;
};

